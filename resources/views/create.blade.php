@extends('layout')

@section('content')

<style>
    .container {
      max-width: 450px;
    }
    .push-top {
      margin-top: 50px;
    }
</style>

<div class="card push-top text-white bg-dark">
  <div class="card-header">
    <h3 style="text-align: center">Tambah Data</h3>
  </div>

  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('pegawais.store') }}">
          <div class="form-group">
              @csrf
              <label for="nama">Nama</label>
              <input type="text" class="form-control" name="nama"/>
          </div>
          <div class="form-group">
              <label for="posisi">Posisi</label>
              <input type="text" class="form-control" name="posisi"/>
          </div>
          <div class="form-group">
              <label for="umur">Umur</label>
              <input type="text" class="form-control" name="umur"/>
          </div>
          <button type="submit" class="btn btn-block btn-secondary">Tambahkan</button>
      </form>
  </div>
</div>
@endsection