@extends('layout')

@section('content')

<style>
    .container {
      max-width: 450px;
    }
    .push-top {
      margin-top: 50px;
    }
</style>

<div class="card push-top text-white bg-dark">
  <div class="card-header">
    Edit Data
  </div>

  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('pegawais.update', $pegawai->id) }}">
          <div class="form-group">
              @csrf
              @method('PATCH')
              <label for="nama">Nama</label>
              <input type="text" class="form-control" name="nama" value="{{ $pegawai->nama }}"/>
          </div>
          <div class="form-group">
              <label for="posisi">Posisi</label>
              <input type="posisi" class="form-control" name="posisi" value="{{ $pegawai->posisi }}"/>
          </div>
          <div class="form-group">
              <label for="umur">Umur</label>
              <input type="text" class="form-control" name="umur" value="{{ $pegawai->umur }}"/>
          </div>
          <button type="submit" class="btn btn-block btn-secondary">SIMPAN</button>
      </form>
  </div>
</div>
@endsection