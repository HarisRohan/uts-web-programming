<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>UTS Web Programming</title>
    <style>
        .header {
            text-align: center;
            font-family: Arial;
            font-weight: bold;
        }
    </style>
</head>
<body>

    <h1 class="header"">CRUD Using Laravel 8 and MySQL</h1>

    <p style="text-align: center"><a href="http://uts-webpro.test/pegawais">Click here to go to the main page</a></p>
    
</body>
</html>