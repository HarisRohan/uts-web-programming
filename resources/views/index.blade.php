@extends('layout')

@section('content')

<style>
  .push-top {
    margin-top: 20px;
    margin-bottom: 20px;
  }
  body {
    background-color : black;
  }
</style>

  <div class="push-top">
    <a href="{{ route('pegawais.create')}}" class="btn btn-info btn-sm">Tambah</a>
  </div>
  <table class="table">
    <thead>
        <tr class="table-info text-uppercase fs-1">
          <th>Nama</th>
          <th>Posisi</th>
          <th>Umur</th>
          <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($pegawai as $pegawais)
        <tr class="table-light">
            <th>{{$pegawais->nama}}</th>
            <td>{{$pegawais->posisi}}</td>
            <td>{{$pegawais->umur}}</td>
            <td class="text-center">
                <a href="{{ route('pegawais.edit', $pegawais->id)}}" class="btn btn-outline-warning btn-sm"">Edit</a>
                <form action="{{ route('pegawais.destroy', $pegawais->id)}}" method="post" style="display: inline-block">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-outline-danger btn-sm"" type="submit">Hapus</button>
                  </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection